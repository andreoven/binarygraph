import React from 'react';
import Chat from './Chat/Chat';
import './App.scss';

function App() {
  return (
    <div className="App">
      <Chat />
    </div>
  );
}

export default App;
