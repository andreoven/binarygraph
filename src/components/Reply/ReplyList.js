import React, { useState } from 'react';
import ReplyItem from './ReplyItem';
import ReplyForm from './ReplyForm';

const ReviewList = props => {
    const [isEdit, setisEdit] = useState(false);
    const { messageId, replies } = props;

    function renderButton() {
        if (isEdit) {
            return (
                <div>
                    <ReplyForm messageId={messageId}  />
                </div>
            )
        } else {
            return (
            <p onClick={() => setisEdit(true)} className="react-button">Reply</p>
            )
        }
    }

    function renderReplies() {
        if (replies) {
            return replies.map(reply => <ReplyItem key={reply.id} text={reply.text} />)
        }
    }

    return (
        <div className="message-reply">
            {renderReplies() }
            {renderButton() }
        </div>
    );
}

export default ReviewList;