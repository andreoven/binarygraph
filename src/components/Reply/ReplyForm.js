import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { MESSAGE_QUERY, POST_REPLY_MUTATION } from '../../queries';

const ReviewForm = props => {
  const { messageId } = props;
  const [text, setText] = useState('');

  const _updateStoreAfterAddingReply = (store, newReply, messageId) => {
    const orderBy = 'createdAt_ASC';
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy
      }
    });
    const repliedMessage = data.messages.messageList.find(
      item => item.id === messageId
    );
      repliedMessage.replies.push(newReply);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    //toggleForm(false);
  };

  return (
    <div className="form-wrapper">
      <div className="input-wrapper">
          <input onChange={e => setText(e.target.value)} value={text}/>
      </div>
      <Mutation
        mutation={POST_REPLY_MUTATION}
        variables={{ messageId, text }}
        update={(store, { data: { postReply } }) => {
            _updateStoreAfterAddingReply(store, postReply, messageId)
        }}
        onCompleted={() => window.location.reload()}
      >
        {postMutation =>
          <button onClick={postMutation}>Post</button>
        }
      </Mutation>
    </div>
  );
};

export default ReviewForm;