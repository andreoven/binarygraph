import React, { useState } from 'react';
import { Query } from 'react-apollo';
import Message from './Message';
import MessageInput from './MessageInput';
import { MESSAGE_QUERY, NEW_MESSAGES_SUBSCRIPTION } from '../../queries';

const ProductList = props => {
  //const orderBy = 'createdAt_ASC';

  const [orderBy, setorderBy] = useState('createdAt_ASC');
  console.log('Chat rerender');

  function setFilter() {

  }

  const _subscribeToNewMessages = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGES_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return {...prev, messages: {
          productList: [newMessage, ...prev.messages.messageList],
          count: prev.messages.productList.length + 1,
          __typename: prev.messages.__typename
        }};
      }
    });
  };

  return (
    <Query query={MESSAGE_QUERY} variables={{ orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>Fetch error</div>;
        _subscribeToNewMessages(subscribeToMore);

        const { messages: { messageList } } = data;

        return (
          <div className="Chat">
            <div className="Messages">
              <p onClick={() => setorderBy('createdAt_DESC')} className="react-button">Sort by date</p>
              <p onClick={() => setorderBy('likes_DESC')} className="react-button">Sort by likes</p>
              <p onClick={() => setorderBy('dislikes_DESC')} className="react-button">Sort by dislikes</p>
              {messageList.map((item, index) => {
                return <Message index = {index} key={item.id} {...item} />
              })}
            </div>
            <div className="Messages-Input">
              <MessageInput />
            </div>
          </div>
        );
      }}
    </Query>
  );
};

export default ProductList;