import React, { useState } from 'react';
import ReplyList from '../Reply/ReplyList'
import { Mutation } from 'react-apollo';
import { UPDATE_MESSAGE_MUTATION,  } from '../../queries';

const Message = props => {
  const { id, text, likes, dislikes, replies, index } = props;

  return (
    <div className="message">
        <div className="message-body">
            <div className="message-text">
                <p>#{index}</p>
                <p>{text}</p>
            </div>
            <div className="message-buttons">
                <Mutation
                    mutation={UPDATE_MESSAGE_MUTATION}
                    variables={{ messageId: id, likes: likes+1, dislikes }}
                >
                    {messageMutation =>
                            <p onClick={messageMutation} className="react-button">Like</p>
                    }
                </Mutation>
                <Mutation
                    mutation={UPDATE_MESSAGE_MUTATION}
                    variables={{ messageId: id, likes, dislikes: dislikes+1 }}
                >
                    {messageMutation =>
                        <p onClick={messageMutation} className="react-button">Dislike</p>
                    }
                </Mutation>
            </div>
            <div className="message-reactions">
                <span>Likes: {likes} </span>
                <span>Dislikes: {dislikes} </span>
            </div>
        </div>
        <ReplyList messageId = {id} replies={replies}/>
    </div>
  );
};

export default Message;