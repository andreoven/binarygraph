import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
      count
      messageList {
        id
        text
        likes
        dislikes
        replies {
          id
          text
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!, $likes: Int!, $dislikes: Int!) {
    postMessage(text: $text, likes: $likes, dislikes: $dislikes) {
      id
      text
      likes
      dislikes
      replies {
        id
        text
      }
    }
  }
`;

export const POST_REPLY_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postReply(messageId: $messageId, text: $text) {
      id
      text
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
      }
    }
  }
`;

export const UPDATE_MESSAGE_MUTATION = gql`
  mutation MessageMutation($messageId: ID!, $likes: Int!, $dislikes: Int!) {
    updateMessage(id: $messageId, likes: $likes, dislikes: $dislikes) {
      id
      likes
      dislikes
    }
  }
`;

// mutation {
//   updateMessage(id: "cjyhiaje9kld90b36qesys0el", likes: 10, dislikes: 20 )
//   {
//     likes
//     dislikes
//   }
// }