function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    text: args.text,
    likes: args.likes,
    dislikes: args.dislikes
  });
}

async function postReply(parent, args, context, info) {
  return context.prisma.createReply({
    text: args.text,
    message: { connect: { id: args.messageId } }
  });
}

function updateMessage(parent, args, context, info) {
  return context.prisma.updateMessage({
    data: {
      dislikes: args.dislikes,
      likes: args.likes,
    },
    where: {
      id: args.id
    }

  });
}

module.exports = {
  postMessage,
  postReply,
  updateMessage
}