import React from 'react';

const ReplyItem = props => {
    const { text } = props;
    return (
        <div className="reply">
            <p>{text}</p>
        </div>
    );
};

export default ReplyItem;