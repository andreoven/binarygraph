import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from '../../queries';

const MessageInput = props => {
  const [text, setText] = useState('');


  const _updateStoreAfterAddingMessage = (store, newMessage) => {
    const orderBy = 'createdAt_ASC';
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy
      }
    });
    data.messages.messageList.unshift(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  return (
    <div className="NewMessage">
        <input type="text" placeholder="Text" value={text} onChange={e => setText(e.target.value)} />
      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ text, likes: 0, dislikes: 0 }}
        update={(store, { data: { postMessage } }) => {
          _updateStoreAfterAddingMessage(store, postMessage);
        }}
        onCompleted={() => window.location.reload()}
      >
        {messageMutation =>
          <p className="react-button" className="post-button" onClick={messageMutation}>Sent</p>
        }
      </Mutation>
    </div>
  );
};

export default MessageInput;